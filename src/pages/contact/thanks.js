import React from "react";
import Layout from "../../components/Layout";

// eslint-disable-next-line
export default () => (
  <Layout>
    <section className="section">
      <div className="container">
        <div className="content">
          <h1>谢谢!</h1>
          <p>表单提交已收到！</p>
        </div>
      </div>
    </section>
  </Layout>
);
